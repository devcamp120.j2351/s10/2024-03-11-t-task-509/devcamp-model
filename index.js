//B1: import thu vien express
const express = require('express')

// Import thư viện mongoose
const mongoose = require('mongoose');

//B2: khoi tao app
const app = new express()

app.use(express.json())

//B3: khai bao cổng
const port = 8000;

//import router
const courseRouter = require('./app/routes/course.route');
const reviewRouter = require('./app/routes/review.route');

// Nhúng tạm thời model để tạo collection
const reviewModel = require("./app/models/review.model");
const courseModel = require("./app/models/course.model");

const middleWare1 = (req, res, next) => {
    console.log(`Middleware 1`);
    next();
}
const middleWare2 = (req, res, next) => {
    console.log(`Middleware 2`);
    next();
}
/** Task 506.10 Application Middleware */
app.use((req, res, next) => {
    let today = new Date();
    console.log(`Time: ${today}`);
    next();
}, (req, res, next) => {
    console.log(`Method: ${req.method}`);
    next();
}, middleWare1)

mongoose.connect("mongodb://localhost:27017/CRUD_Course")
    .then(() => {
        console.log("Kết nối MongoDB thành công!")
    })
    .catch((err) => {
        console.error(err);
    })

/*
app.use((req, res, next) => {
    console.log(`Method: ${req.method}`);
    next();
})*/

app.use('/get-method', (req, res, next) => {
    console.log(`Only get method`);
    next();
})

app.post('/post-method', (req, res, next) => {
    console.log(`Only post method`);
    next();
})

//khai bao api
app.get('/', (req, res) => {
    let today = new Date();
    console.log(today);
    res.json({
        message:`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.get('/get-method',[middleWare1, middleWare2], (req, res) => {
    console.log('Get Method');
    res.json({
        message:'Get Method'
    })
})

app.post('/get-method', (req, res) => {
    console.log('Get Method');
    res.json({
        message:'Get Method'
    })
})


app.post('/post-method', (req, res) => {
    console.log('Post Method');
    res.json({
        message:'Post Method'
    })
})

//Khai báo sủ dụng router
app.use('/courses', courseRouter);
app.use('/reviews', reviewRouter);

//B4: start app
app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})