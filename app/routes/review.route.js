//khai báo thư viện express
const express = require('express')

//khai báo router
const router = express.Router();

//import middleware
const reviewMiddleware = require('../middlewares/review.middleware')

router.use(reviewMiddleware.ReviewCommon);

//khai báo các request
//get all review
router.get('/', reviewMiddleware.GetAllReview, (req, res) => {
    console.log('Get All reviews');
    res.json({
        message:'Get All reviews'
    })
})

//get a review by id
router.get('/:reviewid', reviewMiddleware.GetReviewByID, (req, res) => {
    console.log('Get review by id');
    res.json({
        message:'Get review by id'
    })
})

//create new review
router.post('/', reviewMiddleware.CreateReview, (req, res) => {
    console.log('Create a review');
    res.json({
        message:'Create a review'
    })
})

//update review by id
router.put('/:reviewid', reviewMiddleware.UpdateReviewByID, (req, res) => {
    console.log('Update review by id');
    res.json({
        message:'Update review by id'
    })
})

//delete review by id
router.delete('/:reviewid', reviewMiddleware.DeleteReviewByID, (req, res) => {
    console.log('Delete review by id');
    res.json({
        message:'Delete review by id'
    })
})


module.exports = router;